# MARS Repository #

This repository contains resources used in the development of **MARS: An SDN-Based Malware Analysis Solution**. Next, we show the list of analyzed malwares and, in sequence, the respective reports provided by the sandbox running in MARS infrastructure.


## Analised Malware ##
 
| **Malware SHA1**  | **Clamav Signature**       |
| ------------- | ------------------------------ |
|`ef40b9699a3b9156219b1b1ae1bb920f61cda37d`|` Win.Worm.Allaple-307 `|
|`44fbd1e9d7d0c0a7f3eb5e9de2e640bf5d64e2e2`|` Win.Trojan.Ircbot-1256 `|
|`1ae0c8b0190b05c72b902779f1e471a3bb4feedf`|` Win.Trojan.Perl-35 `|
|`d2fbdd167613e1c851cb6b362d56737bb6f578b8`|` Win.Trojan.Swisyn-1821 `|
|`0320ee1aca89fb00ade98a4d546c8e07dade01ec`|` Win.Trojan.Sdbot-28 `|
|`c33377212dc40200933e346786f2e30ae9665406`|` NO SIGNATURE`|
|`ac7a3bbd2f7da330ff30b7bdd0c60de2b2d873ed`|` Win.Trojan.IRCBot-3485 `|
|`7f4d6745b9053583b55b87bb16a88840e56e5621`|` Win.Trojan.Agent-474236 `|
|`71afc0989a17954d0eb6c480257c378f5d81847a`|` NO SIGNATURE`|
|`1571105b3fcbc117dd7c9308286af30d34787a92`|` NO SIGNATURE`|
|`8351103946b0664ace5384b09a978fdf49c85b90`|` Win.Downloader.74679-1 `|
|`ed47563dd5cc300716a9ba7946424d538f095ce6`|` Win.Downloader.134278-1 `|
|`ee1183fcf26263d45ec1c26ddeccf36ce64b5a25`|` Win.Trojan.Downloader-2215 `|
|`4d5d2bc7a7008ef5f6920495e2ba96b14d2d96d7`|` Win.Trojan.Tomvode-1 `|
|`44627df5595278068872eb1277d2c188907baf94`|` Win.Trojan.Dalbot-1 `|
|`f5f82285141fe18b8b6f592510d85d2d0aa9707f`|` Win.Trojan.Gloox-1 `|
|`39fdf1bfd3b7f1c11ef67be8d07309446fc41aae`|` NO SIGNATURE`|
|`ef6b4afea678eaa80b8ea3ba1fea485c9e678a6e`|` NO SIGNATURE`|
|`9f3d6d3fd87ebb83098e5615e98c6c8e929eab84`|` NO SIGNATURE`|
|`cc91bc68522fa8912825e71abeb2ddf9ffce1220`|` Win.Trojan.Cossta-649 `|
|`ce80c629fda3722449405b7c51857efc7bed0bd7`|` Win.Trojan.Agent-30691 `|
|`e0872ab939c03ccb740fc2aecf97f2bd9c618795`|` Win.Trojan.Agent-385437 `|
|`d70af9deeab81d6cb9263583703049311e4459bd`|` NO SIGNATURE`|
|`6a95860594cd8b7e3636bafa8f812e05359a64ca`|` Win.Trojan.Agent-30723 `|
|`eed4c14d576c9d0a16aabb6516761f8d1eb9160b`|` Win.Trojan.Agent-30583 `|
|`e508966f45b73fe1f9b0ebef3b2501e7845da5ea`|` Win.Trojan.Small-13891 `|
|`745b7a2ad45a1e46daceb20bb182a07522772605`|` Win.Trojan.Agent-474351 `|
|`3f752c5c97e80d41f5498d8a90f5ee8afd1c4372`|` Win.Trojan.Agent-521064 `|
|`4f9ac6ed2eb74a8bb8eb5d23d834c2be6f1ddaf7`|` Win.Trojan.Agent-30696 `|
|`adcead6817f5d0575c09b03eb94a9772584eafae`|` Win.Trojan.Agent-30677 `|
|`fd3c14eb2a27adb8f8085dfed51b98579d8df592`|` Win.Trojan.Agent-30715 `|
|`957c669dda88b8e960c1b1c8f50d8624bccc3f4b`|` Win.Trojan.Agent-30683 `|
|`c35d12f564201f6eee1b76b86610e6e8f64a113b`|` NO SIGNATURE`|
|`48eb49bcdb11b7d27e66def91b5a08b7ecbf1ac2`|` Win.Downloader.83571-1 `|
|`ec13ddb6c448a8fa7619da79beced8d9b19ae416`|` NO SIGNATURE`|
|`a417fd8b203083e5a3ebcb212f661c4aeebc6015`|` NO SIGNATURE`|
|`556aa22e34c05711cece7ca1fc53bbbf2ad977cf`|` Win.Trojan.Scar-989 `|
|`69a0616c005a49fdfd1a0f18cfe87eb5bacae623`|` Win.Trojan.Agent-30718 `|
|`724e06f187271e8cba649d8ce6a72f4036b9b7ab`|` NO SIGNATURE`|
|`3794d50471d7a937241efa60e0b847c65e54c4c8`|` Win.Trojan.Agent-442576 `|
|`c176b323f94759fbcda20a4ccca17993339727c6`|` Win.Trojan.Agent-30623 `|
|`2ca57931c0e0347200f3fed1d188fb4152fb67a5`|` Win.Trojan.Agent-30684 `|
|`32d3604c26a4bb339136119b26ec69c32c156bff`|` Win.Trojan.Agent-400667 `|
|`7a4186dfbb206fc05f4c222a71e09fc56ce3a49e`|` Win.Trojan.Agent-30705 `|
|`4d053b26ad52a8248c80ddde63d4577f27a12dd1`|` Win.Trojan.Agent-30657 `|
|`e90b4c961f4ec67229ebfca4ba72d1eefaedc9cd`|` Win.Trojan.Agent-440760 `|
|`3a93cddb50d96d1472ddedcd8fc2d146ea489ce9`|` Win.Trojan.Downloader-67059 `|
|`aad4b86ad3545c508d5e02a6f78a8f363f9de6dc`|` Win.Downloader.16819-1 `|
|`d7a6662a9be5364b6e5f8fc96e3e0314ef3105e2`|` Win.Trojan.Lithium-3 `|
|`80260c1b637366f301d25cdcda9a7c82e9f09ea5`|` Win.Trojan.Merong-1 `|
|`d20581741e64c8306fa94c7c8605e768a83168d4`|` Win.Downloader.81796-1 `|
|`61796e8d48d366269430054c4e3aeb2bbbec9d29`|` Win.Downloader.133181-1 `|
|`be08b1025dd9d46c7012aa1ae752d4e0af91fc71`|` Win.Trojan.Agent-30710 `|
|`00590628752de21c18d444f625261692c12a287e`|` Win.Trojan.Agent-30575 `|
|`88fc0b30bdafc1093cf873f5a805423d6c9e7225`|` Win.Trojan.Agent-30589 `|
|`4072c65ef571735157453e4c7f68a210df62948f`|` Win.Trojan.Cossta-71 `|
|`0931655cd5eac3abb6d3bace2b8c7e408c65db48`|` Win.Trojan.Downbot-3 `|
|`2872de82a851dbd4fe27b51459c5fb3ccfcdcbcf`|` Heuristics.W32.Polipos.A `|
|`9a6c465a4ccd2a8b6fbab75657f532fdcca27ed0`|` Win.Trojan.Virtob-2 `|
|`c4343f6b7d899ceae825af31209985824e97cd23`|` Win.Worm.Sasser-2 `|
|`7a2b17e11d6b8f31bb261195e2a419bc43ee9e71`|` Win.Trojan.Ircworm-5 `|
|`0d3c49bf681e6128a7c3c663d6eb5701af70759e`|` Win.Trojan.Toa-5372114-0 `|
|`4faf2bc5ae8ec46c89cb305e8dce25a50c8b697c`|` NO SIGNATURE`|
|`a9e2d5f84e9cf5fc4ab585415f3669a474f1c426`|` NO SIGNATURE`|
|`a915c8f23cc2082656522ad06e5c00bbaa0d168f`|` Win.Trojan.Zapchast-2506 `|
|`86ec7431cb4a933b0711be80143c59045d07897e`|` NO SIGNATURE`|
|`7487377374a97a444bdfbeef6177894bb982e034`|` NO SIGNATURE`|
|`58f2e511843d7346f1baba3a77a84a24205696fd`|` NO SIGNATURE`|
|`7534ff6ed4afa03465f41a25e98e6212298c74a6`|` Win.Trojan.Zusy-765 `|
|`b5c75c0be343f5acdf05b239331097f456ed928b`|` NO SIGNATURE`|
|`cccbb4b74fdf7a78b663ec1488f8bc804100dcd4`|` NO SIGNATURE`|
|`325acdf79a66cb744c48363a7d97f19fbaf79c20`|` NO SIGNATURE`|
|`b9de3adc708bc59d806d1f706897b2fa582045a7`|` NO SIGNATURE`|
|`11cd20c734c90d09c8faa5b6e0964d27a666938e`|` NO SIGNATURE`|
|`1164fa9eb0ca28d23b7cd48f731b89f422c2d230`|` NO SIGNATURE`|
|`e3c48e3d1bb0afd39adf5575f8c6d554c8ab93cf`|` NO SIGNATURE`|
|`8d8cc34190831c0b86acc85be5f516261ef60bab`|` NO SIGNATURE`|
|`6c37e484b00f9fd80a92559d16e3de7b68e7b167`|` Win.Trojan.Agent-1316411 `|
|`2aa58c26deac88c63b544318da7ab08e2a5fff97`|` NO SIGNATURE`|
|`3e5879ba2b137f949732c918661e73f18baf0352`|` Win.Trojan.Autoit-2022 `|
|`4d57817bfbf70e06fb7dd5c6a8d340443fd6cfc4`|` NO SIGNATURE`|
|`aa38bef9bb26dcc0b0ebdede1537ddd76665f3f9`|` Win.Trojan.Delf-2270 `|
|`d10f26181ac7491329df4772372a9e8a2d2b1faf`|` Win.Trojan.Refroso-5119 `|
|`30f6a8dba8d73b867c05e16a486f2ca6cdb6e104`|` Win.Worm.Deadhat-1 `|
|`fcbb6d23d43236efe38afbe05c9c803091b8941e`|` Win.Trojan.Poebot-29 `|
|`2ec7139d5fdb02fb1e7557a7339af5c91fb21ed7`|` NO SIGNATURE`|
|`d9a2cf4db9b8cda4190e3eb79f37cf5c8c2dcb9f`|` NO SIGNATURE`|
|`acf9d93e6ebb915d435529102607649a19414bf9`|` NO SIGNATURE`|
|`5b56ec6fbe4a9a948dcfd5ba3d549b09bc4ad08a`|` Win.Trojan.Mybot-11624 `|
|`85555450fb89aca482f74c924504788ff4159323`|` Win.Trojan.Rbot-2443 `|
|`ddb518c695c27f4dbdf77ca969643843cdc8acbe`|` Win.Trojan.Bot-13 `|
|`86f654a312f3247a915f3fa4eea4567513b25628`|` NO SIGNATURE`|
|`d78ae37341a26378a86b460dfaddf054105c25dd`|` NO SIGNATURE`|
|`b7e9c0cef3b1b62f4435850d2c0db0bd6b68b10a`|` NO SIGNATURE`|
|`b3423fef635638bb078b01c34166d1e6eb638d36`|` Win.Trojan.VanBot-1 `|
|`ec56d119dc543082860a6ac1fb8a1374d57e37d8`|` Win.Trojan.IRCBot-3800 `|
|`91a7bb3dbca171f9dd7a793317e06b1b15f8e1a7`|` Win.Trojan.Buzus-4415 `|
|`76e6b13837a6712e3f3f768e3ad2e144fbc28076`|` Win.Trojan.Zloyfly-1 `|
|`42081abf164c948d19075b90e169747f390924f8`|` NO SIGNATURE`|
|`19aee19f5ba66743da4f59858df1cb162841b572`|` NO SIGNATURE`|



## Reports ##

The above malware were analyzed in MARS using the reconfiguration aspects described
in our paper. As result, we present the reports provided by cuckoo sandbox used
in our experiment. The reports are available for [download](https://bitbucket.org/joaoceron/mars/src/b6a9c68c72fdd7ce7fda7b978d63cbca47316be2/mars.experiment.1.analysis/?at=master).
